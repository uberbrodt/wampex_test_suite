defmodule WampexTestSuite do
  @moduledoc """
  Documentation for WampexTestSuite.
  """

  @doc """
  Hello world.

  ## Examples

      iex> WampexTestSuite.hello()
      :world

  """
  def hello do
    :world
  end
end
