defmodule Wampex.Router.AuthenticationImpl do
  @moduledoc false
  @behaviour Wampex.Router.Authentication
  require Logger

  alias Wampex.Crypto
  alias Wampex.Serializers.JSON

  @wampcra "wampcra"
  @auth_provider "userdb"
  @auth_role "user"
  @auth_password "bnASR5qF9y8k/sHF6S+NneCOhvVI0zFkvoKQpc2F+hA="
  @salt "test"
  @keylen 8
  @iterations 1

  @impl true
  def authenticate?(methods) do
    can_auth(methods)
  end

  @impl true
  def method, do: @wampcra

  @impl true
  def challenge(_realm, authid, session_id) do
    now = DateTime.to_iso8601(DateTime.utc_now())

    %{
      challenge:
        JSON.serialize!(%{
          nonce: Crypto.random_string(@keylen),
          authprovider: @auth_provider,
          authid: authid,
          timestamp: now,
          authrole: @auth_role,
          authmethod: @wampcra,
          session: session_id
        }),
      salt: @salt,
      keylen: @keylen,
      iterations: @iterations
    }
  end

  @impl true
  def parse_challenge(challenge) do
    ch = JSON.deserialize!(challenge.challenge)

    {get_in(ch, ["authid"]), get_in(ch, ["authrole"]), get_in(ch, ["authmethod"]),
     get_in(ch, ["authprovider"])}
  end

  @impl true
  def authenticate(signature, realm, authid, %{
        challenge: challenge
      }) do
    authed =
      authid
      |> get_secret(realm)
      |> Crypto.hash_challenge(challenge)
      |> :pbkdf2.compare_secure(signature)

    {authed, %{}}
  end

  defp get_secret(_authid, _uri), do: Crypto.pbkdf2(@auth_password, @salt, @iterations, @keylen)

  defp can_auth([]), do: false
  defp can_auth([@wampcra | _]), do: true
  defp can_auth([_ | t]), do: can_auth(t)
end
