defmodule TestCallee do
  @moduledoc false
  use GenServer
  require Logger
  alias Wampex.Client
  alias Wampex.Roles.{Callee, Dealer}
  alias Wampex.Roles.Peer.Error
  alias Callee.{Register, Yield}
  alias Dealer.Invocation

  def start_link(test, name, device) do
    GenServer.start_link(__MODULE__, {test, name, device})
  end

  def init({test, name, device}) do
    Client.add(name, self())
    {:ok, {test, name, device}}
  end

  def handle_info({:connected, _}, {test, name, device} = state) do
    {:ok, _reg} = Client.register(name, %Register{procedure: "com.actuator.#{device}.light"})
    {:ok, reg} = Client.register(name, %Register{procedure: "return.error"})
    send(test, {:registered, reg})
    {:noreply, state}
  end

  def handle_info(
        %Invocation{
          request_id: id,
          details: %{"procedure" => "return.error"},
          arg_list: al,
          arg_kw: akw
        },
        {_test, name, _device} = state
      ) do
    Client.error(
      name,
      %Error{request_id: id, error: "this.is.an.error", arg_list: al, arg_kw: akw}
    )

    {:noreply, state}
  end

  def handle_info(
        %Invocation{request_id: id, details: _dets, arg_kw: arg_kw} = invocation,
        {test, name, _device} = state
      ) do
    send(test, invocation)

    Client.yield(
      name,
      %Yield{
        request_id: id,
        arg_list: [:ok],
        arg_kw: %{color: Map.get(arg_kw, "color")}
      }
    )

    {:noreply, state}
  end
end
