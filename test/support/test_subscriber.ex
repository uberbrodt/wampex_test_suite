defmodule TestSubscriber do
  @moduledoc false
  use GenServer
  require Logger
  alias Wampex.Client
  alias Wampex.Roles.Subscriber.Subscribe

  def start_link(test, name, topic) do
    GenServer.start_link(__MODULE__, {test, name, topic})
  end

  def init({test, name, topic}) do
    Client.add(name, self())
    {:ok, {test, name, topic}}
  end

  def handle_info({:connected, _client}, {test, name, topic}) do
    {:ok, _sub1} = Client.subscribe(name, %Subscribe{topic: topic})
    {:ok, _sub2} = Client.subscribe(name, %Subscribe{topic: "com.data.test"})
    {:ok, _sub3} = Client.subscribe(name, %Subscribe{topic: "com.data"})

    {:ok, _sub4} =
      Client.subscribe(name, %Subscribe{topic: "com...temp", options: %{"match" => "wildcard"}})

    {:ok, sub5} =
      Client.subscribe(name, %Subscribe{
        topic: "com..test.temp",
        options: %{"match" => "wildcard"}
      })

    send(test, {:subscribed, sub5})
    {:noreply, {test, name, topic}}
  end

  def handle_info(event, {test, _name, _topic} = state) do
    send(test, event)
    {:noreply, state}
  end
end
