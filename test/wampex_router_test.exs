defmodule WampexRouterTest do
  use ExUnit.Case, async: true

  alias Wampex.Client
  alias Wampex.Client.{Authentication, Realm, Session}
  alias Wampex.Roles.Broker.Event
  alias Wampex.Roles.{Callee, Caller, Peer, Publisher, Subscriber}
  alias Wampex.Roles.Caller.Call
  alias Wampex.Roles.Dealer.{Invocation, Result}
  alias Wampex.Roles.Peer.{Error, Hello}
  alias Wampex.Roles.Publisher.Publish
  alias Wampex.Router
  alias Wampex.Router.AuthenticationImpl
  alias Wampex.Router.AuthorizationImpl
  require Logger

  @topologies [
    test: [
      strategy: Cluster.Strategy.Epmd,
      config: [hosts: []]
    ]
  ]

  @url "ws://localhost:5999/ws"
  @authid "admin"
  @auth_password "bnASR5qF9y8k/sHF6S+NneCOhvVI0zFkvoKQpc2F+hA="
  @realm_uri "admin"
  @auth %Authentication{authid: @authid, authmethods: ["wampcra"], secret: @auth_password}
  @realm %Realm{name: @realm_uri, authentication: @auth}
  @roles [Caller, Callee, Publisher, Subscriber]
  @device "as987d9a8sd79a87ds"
  @session %Session{url: @url, realm: @realm, roles: @roles}
  @s Supervisor.child_spec(
       {Router,
        name: TestRouter,
        port: 5999,
        topologies: @topologies,
        replicas: 1,
        quorum: 1,
        authentication_module: AuthenticationImpl,
        authorization_module: AuthorizationImpl},
       id: Test
     )

  defmodule TestStruct do
    defstruct [:test]
  end

  setup do
    start_supervised(@s)
    :ok
  end

  test "struct is map" do
    refute is_map(Client)
  end

  test "role not supported" do
    callee_name = TestCalleeAbortRegistration
    Client.start_link(name: callee_name, session: @session, reconnect: false)
    TestCallee.start_link(self(), callee_name, @device)

    name = TestCallerFail
    [_ | t] = @session.roles
    sess = %Session{@session | roles: t}
    {:ok, _pid} = Client.start_link(name: name, session: sess, reconnect: false)

    Client.call(name, %Call{procedure: "com.actuator.#{@device}.light"})
  catch
    :exit, {:normal, er} -> Logger.error(inspect(er))
    :exit, {:shutdown, er} -> Logger.error(inspect(er))
  end

  test "callee registration" do
    name = TestCalleeRegistration
    Client.start_link(name: name, session: @session, reconnect: false)
    TestCallee.start_link(self(), name, @device)
    assert_receive {:registered, id}, 500
  end

  @tag :abort
  test "abort" do
    callee_name = TestAbort
    {:ok, _pid} = Client.start_link(name: callee_name, session: @session, reconnect: false)
    Client.cast(callee_name, Peer.hello(%Hello{realm: "test", roles: [Callee]}))
  catch
    :exit, er -> assert {:normal, _} = er
  end

  @tag :client
  test "caller receives error when calling unknown procedure" do
    caller_name = TestExistCaller
    Client.start_link(name: caller_name, session: @session, reconnect: false)

    assert %Error{error: "wamp.error.no_registration"} =
             Client.call(
               caller_name,
               %Call{
                 procedure: "this.should.not.exist",
                 arg_list: [1],
                 arg_kw: %{color: "#FFFFFF"}
               }
             )
  end

  @tag :client
  test "caller receives error when callee returns an error" do
    callee_name = TestErrorCalleeRespond
    Client.start_link(name: callee_name, session: @session, reconnect: false)
    TestCallee.start_link(self(), callee_name, @device)
    assert_receive {:registered, id}
    caller_name = TestErrorCaller
    Client.start_link(name: caller_name, session: @session, reconnect: false)

    assert %Error{error: "this.is.an.error", arg_list: [1], arg_kw: %{"color" => "#FFFFFF"}} =
             Client.call(
               caller_name,
               %Call{
                 procedure: "return.error",
                 arg_list: [1],
                 arg_kw: %{color: "#FFFFFF"}
               }
             )
  end

  @tag :client
  test "callee is invoked and responds and caller gets result" do
    callee_name = TestCalleeRespond
    Client.start_link(name: callee_name, session: @session, reconnect: false)
    TestCallee.start_link(self(), callee_name, @device)
    assert_receive {:registered, id}
    caller_name = TestCaller
    Client.start_link(name: caller_name, session: @session, reconnect: false)

    assert %Result{arg_list: ["ok"], arg_kw: %{"color" => "#FFFFFF"}} =
             Client.call(
               caller_name,
               %Call{
                 procedure: "com.actuator.#{@device}.light",
                 arg_list: [1],
                 arg_kw: %{color: "#FFFFFF"}
               }
             )

    assert_receive %Invocation{}
  end

  @tag :client
  test "subscriber registration" do
    name = TestSubscriberRegister
    Client.start_link(name: name, session: @session, reconnect: false)
    TestSubscriber.start_link(self(), name, "com.data.temp")
    assert_receive {:subscribed, id}
  end

  @tag :client
  test "subscriber receives disconnect event" do
    name = TestSubscriberDisconnectEvents
    Client.start_link(name: name, session: @session, reconnect: false)
    TestSubscriber.start_link(self(), name, "router.peer.disconnect")
    assert_receive {:subscribed, id}

    Task.start(fn ->
      {:ok, pid} =
        Client.start_link(name: TestDisconnectPublisher, session: @session, reconnect: false)

      :timer.sleep(200)
      Process.exit(pid, :normal)
    end)

    assert_receive %Event{details: %{"topic" => "router.peer.disconnect"}}, 500
  end

  @tag :client
  test "subscriber receives events from publisher" do
    name = TestSubscriberEvents
    Client.start_link(name: name, session: @session, reconnect: false)
    TestSubscriber.start_link(self(), name, "com.data.test.temp")

    Client.start_link(name: TestPublisher, session: @session, reconnect: false)

    Client.publish(
      TestPublisher,
      %Publish{
        topic: "com.data.test.temp",
        arg_list: [12.5, 45.6, 87.5],
        arg_kw: %{loc: "60645"}
      }
    )

    assert_receive %Event{details: %{"topic" => "com.data.test.temp"}}
    assert_receive %Event{details: %{"topic" => "com.data.test.temp"}}
    assert_receive %Event{details: %{"topic" => "com.data.test.temp"}}
  end
end
